package aplicacion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sergio
 */
public class TDireccion {
	private String calle;
	private short num;
	private String ciudad;
	private String cp;
	private String pais;

	public TDireccion(String calle, short num, String ciudad, String cp, String pais) {
		this.calle = calle;
		this.num = num;
		this.ciudad = ciudad;
		this.cp = cp;
		this.pais = pais;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public short getNum() {
		return num;
	}

	public void setNum(short num) {
		this.num = num;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	// (calle,30,Alcoi,03802,España)
	@Override
	public String toString() {
		return "(" + this.calle + "," + this.num + "," + this.ciudad + "," + this.cp + "," + this.pais + ")";
	}
}
