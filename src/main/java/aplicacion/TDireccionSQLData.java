package aplicacion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 *
 * @author sergio
 */
// Esta sería la forma de trabajar pero postgresql no lo implementa y, por tanto, no funciona.
public class TDireccionSQLData extends TDireccion implements SQLData {
	private String sqlTypeName;

	public TDireccionSQLData(String calle, short num, String ciudad, String cp, String pais) {
		super(calle, num, ciudad, cp, pais);
	}

	public String getSQLTypeName() throws SQLException {
		return sqlTypeName;
	}

	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		sqlTypeName = typeName;
		setCalle(stream.readString());
		setNum(stream.readShort());
		setCiudad(stream.readString());
		setCp(stream.readString());
		setPais(stream.readString());

	}

	public void writeSQL(SQLOutput stream) throws SQLException {
		stream.writeString(getCalle());
		stream.writeShort(getNum());
		stream.writeString(getCiudad());
		stream.writeString(getCp());
		stream.writeString(getPais());

	}

}
