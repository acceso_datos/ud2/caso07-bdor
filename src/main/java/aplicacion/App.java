package aplicacion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

//import java.util.HashMap;
//import java.util.Map;
//import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.util.PGobject;

public class App {

	public static void main(String[] args) throws SQLException {
		Connection con = ConexionBD.getConexion();
		if (con != null) {
			Statement st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// Ejemplo consulta1
			System.out.println("Ejemplo de Consulta1");
			ResultSet rs = st.executeQuery("SELECT * FROM bdor.cliente");
			// Tipos de datos
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			for (int i = 1; i <= columnCount; i++) {
				String s = rsmd.getColumnTypeName(i);
				System.out.println("Columna " + i + " es de tipo: " + s);
			}
//			mostrarDatos(rs);

			// Ejemplo consulta2
			System.out.println("Ejemplo de Consulta2");
			rs = st.executeQuery("SELECT * FROM bdor.cliente WHERE 'email2@ad.es' = any(emails)");
			mostrarDatos(rs);

			// Inserción de datos
			System.out.println("Insertando registro...");
			rs.moveToInsertRow();
			rs.updateString("nombre", "Edgarx");
			rs.updateString("apellidos", "Frank Codd");

//        	// composite type. 
			// Forma1: en el driver postgresql no viene implementado y, por tanto, no
			// funcionaría
//        	// Si funcionara se haría así:
//			Map<String, Class<?>> m = new HashMap<String, Class<?>>();
//			m.put("tlista_telefonos", TListaTelefonosSQLData.class);
//			m.put("tdireccion", TDireccionSQLData.class);
//			con.setTypeMap(m);
//			
//			TDireccionSQLData td = new TDireccionSQLData("C/ Datos", (short) 30, "Alcoi", "03802", "España");			
//			rs.updateObject("direccion", td);
//			
//			String[] tfnos = { "123456789", "987654321" };
//			TListaTelefonosSQLData tlt = new TListaTelefonosSQLData("0034", tfnos);
//			rs.updateObject("telefonos", tlt);

			// // Forma2. Otra opción es usar PGObject
			PGobject pgo = new PGobject();
			TDireccion td = new TDireccion("Datos", (short) 30, "Alcoi", "03802", "España");
			pgo.setType("tdireccion");
			pgo.setValue(td.toString());
			rs.updateObject("direccion", pgo);

			String[] tfnos = { "123456789", "987654321" };
			TListaTelefonos tlt = new TListaTelefonos("0034", tfnos);
			pgo = new PGobject();
			pgo.setType("tlista_telefonos");
			pgo.setValue(tlt.toString());
			rs.updateObject("telefonos", pgo);

			String[] v_emails = { "a@a.com", "b@b.com", "c@c.com" };
			java.sql.Array emails = con.createArrayOf("varchar", v_emails);
			rs.updateArray("emails", emails);

			rs.insertRow();
			System.out.println("Registro insertado correctamente!");

			rs.close();

		}
	}

	private static void mostrarDatos(ResultSet rs) throws SQLException {
		System.out.println("------------------------- LISTADO -------------------------");
		java.sql.Array emails = null;
		String[] v_emails;
		rs.beforeFirst();
		while (rs.next()) {
			System.out.print(rs.getInt(1) + " - ");
			System.out.println("\t" + rs.getString(2) + " " + rs.getString(3));
			System.out.println("\t" + rs.getString(4));
			System.out.println("\t" + rs.getString(5));

			// System.out.println(rs.getArray(6)); o directamente rs.getString(6);
			emails = rs.getArray(6);
			v_emails = (String[]) emails.getArray();
			System.out.print("\t");
			for (String e : v_emails) {
				System.out.print(e + " ");
			}
			System.out.println();
		}
		System.out.println("------------------------- FIN LISTADO -------------------------"
				+ System.getProperty("line.separator"));
	}

}

// DOCUMENTACION postgresql           
//            Arrays
//              https://www.postgresql.org/docs/14/arrays.html
//            Tipos compuestos (composite-types) 
//              https://www.postgresql.org/docs/14/rowtypes.html 
//            Herencia 
//              https://www.postgresql.org/docs/14/static/ddl-inherit.html 
//         

// 	ESTRUCTURA del schema 'bdor' : 
//			  CREATE SCHEMA bdor;
//            CREATE TYPE bdor.tlista_telefonos AS
//            (
//                prefijo char(4),	
//                telefonos char(9) ARRAY
//            );
//            CREATE TYPE bdor.tdireccion AS
//            (
//                calle character varying(60),
//                numero smallint,
//                localidad character varying(30),
//                cpostal char(5),
//                pais varchar(30)
//            );
//            CREATE TABLE bdor.cliente
//            (
//                cod bigserial PRIMARY KEY,
//                nombre character varying(20),
//                apellidos varchar(40),
//                direccion tdireccion,
//                telefonos tlista_telefonos,
//                emails varchar(50) ARRAY           
//            );       
//           INSERT INTO bdor.cliente (nombre, apellidos, direccion, telefonos, emails) 
//            VALUES ('Richard','Matthew Stallman', ROW('Serreta',10, 'Alcoi', '03430', 'España'), 
//            ROW('0034',ARRAY['111111111','222222222','333333333']), 
//            ARRAY['email1@ad.es', 'email2@ad.es'] 
//           );
//         
//
// EJERCICIOS
//        // Ejercicio1
//        // Realiza una consulta sobre la tabla CLIENTE (creada anteriormente) que muestre el nombre, 
//        // los apellidos, la calle y la localidad de todos los clientes.
//        
//        // Ejercicio2
//        // Realiza una consulta sobre la tabla CLIENTE (creada anteriormente) que muestre el nombre, los apellidos,  
//        // el prefijo y el primer telefono de la lista de telefonos.
//        
//        // Ejerccio3
//        // Realiza una actualización en la dirección del cliente Richard, Concretamente en su localidad. 
//		  // Hazlo usando dml y resultset.
//        // Ahora deberá ser Valencia.
//        
//        // Ejercicio4
//        // Realiza una actualización en los telefonos del cliente Richard.
//		  // Hazlo usando dml y resultset.
//        // Debemos cambiar el prefijo a 0035 y también su primer y segundo telefono a 123123123 y 321321321, respectivamente.
//    }
//}
