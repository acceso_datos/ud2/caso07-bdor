package aplicacion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 *
 * @author sergio
 */
// Esta sería la forma de trabajar pero postgresql no lo implementa y no funciona.
public class TListaTelefonosSQLData extends TListaTelefonos implements SQLData {
	private String sqlTypeName;

	public TListaTelefonosSQLData(String prefijo, String[] telefonos) {
		super(prefijo, telefonos);
	}

	public String getSQLTypeName() throws SQLException {
		return sqlTypeName;
	}

	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		sqlTypeName = typeName;
		setPrefijo(stream.readString());		
		setTelefonos((String[]) stream.readArray().getArray());
	}

	public void writeSQL(SQLOutput stream) throws SQLException {
		stream.writeString(getPrefijo());
		String[] tfnos = getTelefonos();
		stream.writeArray(ConexionBD.getConexion().createArrayOf("varchar", tfnos));
	}

}
