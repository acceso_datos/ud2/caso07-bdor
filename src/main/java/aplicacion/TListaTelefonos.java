package aplicacion;

/**
 *
 * @author sergio
 */
public class TListaTelefonos {

	private String prefijo;
	private String[] telefonos;

	public TListaTelefonos(String prefijo, String[] telefonos) {
		this.prefijo = prefijo;
		this.telefonos = telefonos;
	}

	public String[] getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String[] telefonos) {
		this.telefonos = telefonos;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	// (0034,"{111111111,222222222,333333333}")
	@Override
	public String toString() {
		String tel = "\"{";
		for (int i = 0; i < telefonos.length; i++) {
			tel = tel + telefonos[i];
			if (i < telefonos.length - 1)
				tel = tel + ",";
		}
		tel = tel + "}\"";
		return "(" + prefijo + "," + tel + ")";
	}

}
